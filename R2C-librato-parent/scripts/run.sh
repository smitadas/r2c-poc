#!/bin/bash
set -x

# run the R2C services in development mode

DEVELOPMENT_CLASSPATH=\
$PWD/..:\
$PWD/dto/target/classes:\
$PWD/dao/target/classes:\
$PWD/service/target/classes:\
$PWD/service/target/R2C-librato-service-0.0.1-SNAPSHOT.jar

CONFIG_FILE=service/r2c-librato.yml

JVM_SETTINGS="\
 -server -Xms1024m -Xmx1024m -XX:MaxPermSize=256m\
 -XX:+UseParNewGC -XX:MaxNewSize=384m -XX:NewSize=384m\
 -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=30 -XX:+UseTLAB\
 -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+CMSPermGenSweepingEnabled"

java $JVM_SETTINGS -classpath $DEVELOPMENT_CLASSPATH \
	 -Dcom.sungardas.home=/usr/local/sg/chaldron \
     com.sungardas.r2c.librato.ws.R2CLibratoService server $CONFIG_FILE
