@echo ON
setlocal
set R2C_DIR="%~dp0.."
set CLASSPATH=%R2C_DIR%\..;%R2C_DIR%\R2C-librato-dto\target\classes;%R2C_DIR%\R2C-librato-dao\target\classes;%R2C_DIR%\R2C-librato-service\src\main\resources;%R2C_DIR%\R2C-librato-service\target\classes;%R2C_DIR%\R2C-librato-service\target\R2C-librato-service-2.5.0.jar

set JVM_SETTINGS=^
 -server -Xms1024m -Xmx1024m -XX:MaxPermSize=256m^
 -XX:+UseParNewGC -XX:MaxNewSize=384m -XX:NewSize=384m^
 -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=30 -XX:+UseTLAB^
 -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+CMSPermGenSweepingEnabled

java %JVM_SETTINGS% -Dcom.sungardas.home=C:\sg\chaldron -classpath %CLASSPATH% com.sungardas.r2c.librato.ws.R2CLibratoService server r2c-service\r2c-librato.yml

endlocal
