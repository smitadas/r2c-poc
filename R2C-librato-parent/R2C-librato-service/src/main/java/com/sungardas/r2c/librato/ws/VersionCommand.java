package com.sungardas.r2c.librato.ws;

import net.sourceforge.argparse4j.inf.Namespace;

import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.cli.EnvironmentCommand;
import com.yammer.dropwizard.config.Configuration;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.config.ServerFactory;

public class VersionCommand<T extends Configuration> extends EnvironmentCommand<T> {
    private final Class<T> configurationClass;

    public VersionCommand(Service<T> service) {
        super(service, "version", "Get the service version.");
        this.configurationClass = service.getConfigurationClass();
    }

    /*
     */
    @Override
    protected Class<T> getConfigurationClass() {
        return configurationClass;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T configuration) throws Exception {
        final Server server = new ServerFactory(configuration.getHttpConfiguration(),
                                                environment.getName()).buildServer(environment);
        final Logger logger = LoggerFactory.getLogger(VersionCommand.class);
        try {
            System.out.println("Implementation Version");
            System.out.println("----------------------");
            System.out.println(VersionCommand.class.getPackage().getImplementationVersion());
        } catch (Exception e) {
            logger.error("Unable to get version");
        }
    }
}
