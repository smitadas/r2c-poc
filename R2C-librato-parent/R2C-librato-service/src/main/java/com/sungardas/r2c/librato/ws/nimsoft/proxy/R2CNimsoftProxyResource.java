package com.sungardas.r2c.librato.ws.nimsoft.proxy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.NotFoundException;
import com.sungardas.r2c.monitoring.dto.R2CCustomer;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.yammer.dropwizard.hibernate.UnitOfWork;
import com.yammer.dropwizard.jersey.params.LongParam;
import com.yammer.metrics.annotation.Timed;

@Path("/r2cnimsoftproxy.json")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/r2cnimsoftproxy", description = "Data fetch from nimsoft.")
public class R2CNimsoftProxyResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(R2CNimsoftProxyResource.class);

    //TODO : create resource endpoints here. 
    
//    @GET 
//    @Path("/customer/{customerOracleId}")
//    @ApiOperation(value = "Get details of the R2CCustomer", 
//	responseClass = "com.sungardas.adapter.r2c.monitoring.dto.R2CCustomer")
//	@Timed(name = "get-requests")
//	@UnitOfWork
//	public R2CCustomer getR2CCustomer(@ApiParam(value = "Customer Oracle ID.", required = true)  
//	    @PathParam("customerOracleId") LongParam oracleId) throws Exception {
//	if (null == oracleId || oracleId.get() == 0)
//	    throw new NotFoundException("Bad value of oracle id = " + oracleId);
//	R2CCustomer r2cCustomer = R2CMonitoringUtil.getR2CCustomerByOracleId(oracleId.get());
//	if (null == r2cCustomer) {
//	    throw new NotFoundException("No customer with oracle id = " + oracleId + "found. ");
//	}
//	return r2cCustomer;
//    }    

}
