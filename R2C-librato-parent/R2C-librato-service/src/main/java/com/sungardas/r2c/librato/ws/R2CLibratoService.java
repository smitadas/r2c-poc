package com.sungardas.r2c.librato.ws;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sungardas.r2c.librato.ws.nimsoft.proxy.R2CNimsoftProxyResource;
import com.sungardas.r2c.ws.entity.OracleIdRxCustomerIdMap;
import com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.assets.AssetsBundle;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.ConfigurationException;
import com.yammer.dropwizard.config.ConfigurationFactory;
import com.yammer.dropwizard.config.Environment;
import com.yammer.dropwizard.config.FilterBuilder;
import com.yammer.dropwizard.db.DatabaseConfiguration;
import com.yammer.dropwizard.hibernate.HibernateBundle;
import com.yammer.dropwizard.jersey.DropwizardResourceConfig;
import com.yammer.dropwizard.json.ObjectMapperFactory;
import com.yammer.dropwizard.validation.Validator;

public class R2CLibratoService  extends Service<R2CLibratoConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(R2CLibratoService.class);
    
    
    private static String configFileName;

    public static void main(String[] args) throws Exception {
        configFileName = args[1];
        new R2CLibratoService().run(args);
    }
    private final HibernateBundle<R2CLibratoConfiguration> hibernateBundle =
        new HibernateBundle<R2CLibratoConfiguration>(
            OracleIdRxCustomerIdMap.class) {
        @Override
        public DatabaseConfiguration getDatabaseConfiguration(R2CLibratoConfiguration configuration) {
           return configuration.getDatabaseConfiguration();
        }
    };
    
    
    
    @Override
    public void initialize(Bootstrap<R2CLibratoConfiguration> bootstrap) {
        try {
            final R2CLibratoConfiguration configuration = parseConfiguration(configFileName, 
                    bootstrap.getObjectMapperFactory().copy());
            bootstrap.setName("r2");
            if (!configuration.getProductionMode()) {
                // swagger API
                bootstrap.addBundle(new AssetsBundle("/api"));

                // UI path for development
                bootstrap.addBundle(new AssetsBundle("/Portal/Portlet/recover-to-cloud-portlet/src/main/webapp/", "/r2c/", "dev.html"));
                bootstrap.addBundle(new AssetsBundle("/Portal/Theme/chaldron-theme/src/main/webapp/css/", "/css/"));
                bootstrap.addBundle(new AssetsBundle("/Portal/Theme/chaldron-theme/src/main/webapp/js/", "/js/"));
                bootstrap.addBundle(new AssetsBundle("/Portal/Theme/chaldron-theme/src/main/webapp/images/", "/images/"));
                bootstrap.addBundle(new AssetsBundle("/Portal/Theme/chaldron-theme/src/main/webapp/templates/", "/templates/"));
            }
            bootstrap.addBundle(hibernateBundle);
            bootstrap.addCommand(new VersionCommand<R2CLibratoConfiguration>(this));
        } catch (Exception e) {
            e.printStackTrace(); 
            System.exit(1);
        }
	
    }

    
    private R2CLibratoConfiguration parseConfiguration(String filename,
            ObjectMapperFactory objectMapperFactory) throws IOException, ConfigurationException {
        final ConfigurationFactory<R2CLibratoConfiguration> configurationFactory =
            ConfigurationFactory.forClass(R2CLibratoConfiguration.class, new Validator(), objectMapperFactory);
        final File file = new File(filename);
        if (!file.exists()) {
            throw new FileNotFoundException("File " + file + " not found");
        }
        return configurationFactory.build(file);
    }




    @Override
    public void run(R2CLibratoConfiguration configuration, Environment environment) throws Exception {
	R2CLibratoConfiguration.instance = configuration;
//        DAOs.initialize(new OracleIdRxCustomerIdMapDAO(hibernateBundle.getSessionFactory()));
        FilterBuilder filterConfig = environment.addFilter(CrossOriginFilter.class, "*");
        // 1 day - jetty-servlet CrossOriginFilter will convert to Int.
        filterConfig.setInitParam(CrossOriginFilter.PREFLIGHT_MAX_AGE_PARAM, String.valueOf(60*60*24)); 
        filterConfig.setInitParam("org.eclipse.jetty.servlet.Default.dirAllowed", "true");
        environment.addResource(new R2CNimsoftProxyResource());
//        environment.addResource(new RxMonitoringResource());

        if (!configuration.getProductionMode()) {
            System.out.println("ApiListingResourceJSON");
            environment.addResource(new ApiListingResourceJSON());
        }else 
            System.out.println("NO ApiListingResourceJSON");

        environment.setJerseyProperty(DropwizardResourceConfig.PROPERTY_CONTAINER_RESPONSE_FILTERS, 
                "com.sun.jersey.server.linking.LinkFilter"); 
    }

}