package com.sungardas.r2c.librato.ws;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yammer.dropwizard.config.Configuration;
import com.yammer.dropwizard.db.DatabaseConfiguration;

public class R2CLibratoConfiguration  extends Configuration {
    public static R2CLibratoConfiguration instance;

    static class DevMode {
        @Valid
        @NotNull
        @JsonProperty("enabled")
        private boolean enabled;

        public boolean getEnabled() {
            return enabled;
        }
    }

    static class RXApi {
        @Valid
        @NotNull
        @JsonProperty("enabled")
        private boolean enabled;

        @Valid
        @NotNull
        @JsonProperty("url")
        private String url;
        
        @Valid
        @NotNull
        @JsonProperty("winPath")
        private String winPath;
        
        @Valid
        @NotNull
        @JsonProperty("linuxPath")
        private String linuxPath;
        
        @Valid
        @NotNull
        @JsonProperty("rxUserName")
        private String rxUserName;
        
        @Valid
        @NotNull
        @JsonProperty("rxPassword")
        private String rxPassword;
        
        @Valid
        @JsonProperty("accessSignature")
        private String accessSignature;
        
        public String getUrl() {
        	return url;
        }
        
        public boolean getEnabled() {
            return enabled;
        }
        
        public String getWinPath() {
        	return winPath;
        }
        
        public String getLinuxPath() {
        	return linuxPath;
        }
        
        public String getRxUserName() {
        	return rxUserName;
        }
        
        public String getRxPassword() {
        	return rxPassword;
        }
        
        public String getAccessSignature() {
        	return accessSignature;
        }
    }

    @Valid
    @JsonProperty("devMode")
    private DevMode devMode = new DevMode();

    public DevMode  getDevMode() {
        return this.devMode;
    }

    public boolean getProductionMode() {
        return this.devMode == null  || !this.devMode.getEnabled();
    }

    @Valid
    @JsonProperty("rxAPI")
    private RXApi rxAPI = new RXApi();

    public RXApi  getDevrxAPI() {
        return this.rxAPI;
    }

    public boolean getUseRxAPI()
    {
    	return this.rxAPI.getEnabled();
    }
    
    public String getUxurl(){
    	return this.rxAPI.getUrl();
    }
    
    public String getWinPath()
    {
    	return this.rxAPI.getWinPath();
    }
    
    public String getLinuxPath() {
    	return this.rxAPI.getLinuxPath();
    }
    
    public String getRxUserName() {
    	return this.rxAPI.getRxUserName();
    }
    
    public String getRxPassword() {
    	return this.rxAPI.getRxPassword();
    }
    
    public String getAccessSignature() {
    	return this.rxAPI.getAccessSignature();
    }
    
    @Valid
    @NotNull
    @JsonProperty("database")
    private DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();

    public DatabaseConfiguration getDatabaseConfiguration() {
        return databaseConfiguration;
    }
    public void setDatabaseConfiguration(DatabaseConfiguration databaseConfiguration) {
        this.databaseConfiguration = databaseConfiguration;
    }
    
    
}
